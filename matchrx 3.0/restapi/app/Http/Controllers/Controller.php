<?php
namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Validation\ValidationException;

class Controller extends BaseController
{
    public $sort = 'id';
    
    public $sortby = 'asc';
    
    public $perpage = 10;
    
    public function __construct()
    {
        
    }

    /*
     * Hashing A Password Using Bcrypt
     */
    protected function _hashPwd($pwd)
    {
      return Hash::make($pwd);
    }
    
    /*
     * Hashing A Password Using Bcrypt
     */
    protected function _verifyPwd($pwd,$hashedPassword)
    {
        return (Hash::check($pwd, $hashedPassword)) ? true : false;      
    }

    /*
     * Encrypt and Decrype the Message 
     */
    protected function _crypt($val,$type=null)
    {
        if(!null === $type) return Crypt::decrypt($val);
        return Crypt::encrypt($val);
    }

    /* 
     * Get Table Columns
     */
    protected function getTableColumns($tablename){
        return DB::getSchemaBuilder()->getColumnListing($tablename);
    }
  
    /*
     * Get Table Columns
     */
    protected function qSearch($q,$tbl)
    {
        $this->getTableColumns($tbl);
    }
  
    /* 
     *  Function Converting Object to Array
     */
    protected function qParamObj2Arr($q)
    { 
        $farr = $evenVal = [];
        $q = explode(',',str_replace(['{','}',':',',','"',"'"],['','',',',',','',''],trim($q)));
        $i=0;        
        foreach($q as $val){     
            if (($i % 2) == 0){               
                array_push($evenVal,$val);
            } else {            
                array_push($farr,[trim($evenVal[0]),'=',trim($val)]);
                $evenVal = [];
            }
            $i++;
        }        
        return $farr;       
    }

    /* 
     *  Sort - Default id
     *  Sort by - Default ASC
     *  Limit - Default 10
     *  Filter - Field and value
     */
    protected function inputFilters($inp = [],$tbl = null){        
        return [
          'sort'    => isset($inp['sort']) ? $inp['sort'] : $this->sort,
          'sortby'  => isset($inp['sortby']) ? strtolower($inp['sortby']) : strtolower($this->sortby),
          'perpage' => isset($inp['limit']) && (int)$inp['limit'] ? $inp['limit'] : (int)$this->perpage,
          'filter'  => isset($inp['filter']) ? $this->qParamObj2Arr($inp['filter']) : [],
          //'q' => isset($inp['q']) ? $this->qSearch($inp['q'],$tbl) : []
        ];         
    }
  
    /*
     *  Code - 200
     *  Success Response     
     */
    protected function successResponse($data) {	
        $response = [
            'code' => 200,
            'status' => 'success',
            'data' => $data
        ];
        return response()->json($response, $response['code']);
    }

    /*
     *  Code - 201
     *  Success Response     
     */
     protected function createdResponse($data) {	
        $response = [
            'code' => 201,
            'status' => 'success',
            'data' => $data
        ];
        return response()->json($response, $response['code']);
    }

    /*
     *  Code - 404
     *  Error Response - Resource not found   
     */
    protected function notFoundResponse() {	
        $response = [
          'code' => 404,
          'status' => 'error',
          'data' => 'Resource Not Found',
          'message' => 'Not Found'
        ];
        return response()->json($response, $response['code']);
    }

    /*
     *  Code - 204
     *  Success Response - After the data is deleted   
     */
     protected function deleteResponse() {	
        $response = [
          'code' => 200,
          'status' => 'success',
          'data' => [],
          'message' => 'Resource Deleted'
        ];
        return response()->json($response, $response['code']);
    }

    /*
     *  Code - 422
     *  Error Response - Unprocessable Entity
     */
     protected function errorResponse($data) {	        
        $response = [
          'code' => 422,
          'status' => 'error',
          'data' => $data,
          'message' => 'Unprocessable Entity' 
        ];          
        echo json_encode($response); //exit;       
        //return response()->json($response, $response['code']);
    }

    /*
     *  Lumen Custom Error Messages    
     *  Error Response - Unprocessable Entity
     */
    protected function errorMessage(){
        return [
            'required'  => 'The :attribute field is required.',
            'numeric'   => 'The :attribute field should be number.',
            'alpha'     => 'The :attribute field should be alphabetic characters.',
            'string'    => 'The :attribute field should be a string.',
            'alpha_num' => 'The :attribute field should be alpha-numeric characters.'                   
        ];
    }

    /*
     *  Lumen Custom Error Messages
     *  Code - 422
     *  Error Response - Unprocessable Entity
     */
    protected function slug_input($input){
        return str_slug($input, "-");  
    }
}
