<?php

namespace App\Http\Controllers;
use DB;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Hash;

use Validator;
use Exception;
use Illuminate\Validation\ValidationException;  
  
class UserController extends Controller{       

    public function __construct()
    {
        
    }

    /*
     * Validate the  User inputs  * 
     */
     protected function validateRules($request){
        $rules = [
            'email' => 'required|email|max:255|unique:users',
            'username' => 'required|max:255|unique:users',
            'password' => 'required|min:6',            
        ];         
        $validator = Validator::make($request, $rules, $this->errorMessage());        
        if($validator->fails()) {          
            return $this->errorResponse($validator->errors());                
        }
        return true; 
    }

    /*
     * @param $request - Requests the inputs
     *  Get All user details
     */
    public function index(Request $request){
        try{ 
            $r = $this->inputFilters($request->all());   
            $users  = User::where($r['filter'])->orderBy($r['sort'], $r['sortby'])->paginate($r['perpage']); 
            return $this->successResponse($users, 200);
            
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }
    }
    
    /*
     * @param $request - Requests the inputs
     * User Login
     */
    public function login(Request $request){             
        try{            
            /* $res = $this->preDefinedInputRequest($request->all());        
            $Page  = Page::where($res['filter'])->orderBy($res['sort'], $res['sortby'])->paginate($res['perpage']);                
            return $this->successResponse($Page); */
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }           
    }

    /*
     * @param $request - Requests the inputs
     * User Register
     */
    public function register(Request $request){
        try{           
            if($this->validateRules($request->all()) === true){                    
                $request->request->add(['password' => Hash::make($request->input('password')) ]);                    
                $user = User::create($request->all());                     
                return $this->createdResponse($user);
            }           
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }
    }

    public function update(Request $request, $id){        
        $user = User::find($id);
		if(!$user){
            return $this->notFoundResponse();
        }
        try{
            if($this->validateRules($request->all()) === true){
                $user->email 		= $request->input('email');
                $user->username 	= $request->input('username');                
                $user->password 	= Hash::make($request->input('password'));
                $user->save();                
                return $this->successResponse($user);
            }
        }catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        } 
	}
    /*
     * @param $id
     * Fetch the records by $id
     */
     public function getId($id){       
        try{        
            if ($user  = User::find($id)) return $this->successResponse($user);
            return $this->notFoundResponse();
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }
    }

    /*
     * @param $id
     * delete the record by $id
     */
     public function delete($id){      
        if (!$User  = User::find($id)) 
            return $this->notFoundResponse();		 
        try{               
            $User->delete(); 
            return $this->deleteResponse();
        } catch(\Exception $exception){
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }         
    }

    
}
