<?php

namespace App\Http\Controllers;
use App\Order;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Illuminate\Validation\ValidationException;
  
  
class OrderController extends Controller{    
    
    /*
     * Validate the inputs  * 
     */
    protected function orderValidateRules($request){
        $rules = [
            'order_currency' => 'required|alpha',
            'order_status' => 'required|alpha',
            'return_label' => 'required|alpha',
            'exchange_label' => 'required|alpha',
            'seller_reconfirm' => 'required|alpha',
            'buyer_cancelled' => 'required|alpha',
            'order_date' => 'required|date_format:"Y-m-d H:i"',
            'buyer_name' => 'required|alpha_num',
            'buyer_state' => 'required|alpha',
            'seller_name' => 'required|alpha_num',
            'seller_state' => 'required|alpha',
            'seller_ar' => 'required|string',
            'product_cost' => 'required|numeric',
            'fee_seller' => 'required|numeric',
            'ship_buyer' => 'required|numeric',
            'ship_seller' => 'required|numeric',
            'total_shipment' => 'required|numeric',
            'buyer_total' => 'required|numeric',
            'seller_total' => 'required|numeric',
            'ship_method' => 'required|alpha_num',
            'tracking' => 'required|numeric',
            'last_shipment_status' => 'required|string',
            'shipment_status_time' => 'required|date_format:"Y-m-d H:i"',                
        ];       
        $validator = Validator::make($request, $rules, $this->errorMessage());       
        if($validator->fails()) {                
            $messages = $validator->errors();
            return $this->errorResponse($validator->errors());                
        }
        return true;   
    }
    
    
    /*
     * @param $request - Requests the inputs
     * Fetch all the records
     */
    public function index(Request $request){
        try{            
            $r = $this->inputFilters($request->all());                 
            $Order  = Order::where($r['filter'])->orderBy($r['sort'], $r['sortby'])->paginate($r['perpage']);                
            return $this->successResponse($Order);
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }       
    }

    /*
     * @param $id
     * Fetch the records by $id
     */
    public function getId($id){        
        try{
            if ($Order  = Order::find($id)) return $this->successResponse($Order);
            return $this->notFoundResponse();
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }
    }    
  
    /*
     * @param $request
     * push the $request data in the table
     */
    public function create(Request $request){
        try{            
            if($this->orderValidateRules($request->all()) === true){
              $Order = Order::create($request->all());  
              return $this->createdResponse($Order);
            }             
        } catch(\Exception $exception){
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        } 
    }
  
    /*
     * @param $id
     * delete the record by $id
     */
    public function delete($id){            
        if (!$Order  = Order::find($id)) 
            return $this->notFoundResponse();		 
        try{               
            $Order->delete(); 
            return $this->deleteResponse();
        } catch(\Exception $exception){
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }         
    }
  
    /*
     * @param $id
     * @param $request
     * update $request by $id
     */
    public function update(Request $request,$id){       
        if (!$Order  = Order::find($id)) {
			return $this->notFoundResponse();
        }          
        try{            
            if($this->orderValidateRules($request->all()) === true){                             
                $Order->order_currency = $request->input('order_currency');
                $Order->order_status = $request->input('order_status');
                $Order->return_label = $request->input('return_label');
                $Order->exchange_label = $request->input('exchange_label');
                $Order->seller_reconfirm = $request->input('seller_reconfirm');
                $Order->buyer_cancelled = $request->input('buyer_cancelled');
                $Order->order_date = $request->input('order_date');
                $Order->buyer_name = $request->input('buyer_name');
                $Order->buyer_state = $request->input('buyer_state');
                $Order->seller_name = $request->input('seller_name');
                $Order->seller_state = $request->input('seller_state');
                $Order->seller_ar = $request->input('seller_ar');
                $Order->product_cost = $request->input('product_cost');
                $Order->fee_seller = $request->input('fee_seller');
                $Order->ship_buyer = $request->input('ship_buyer');
                $Order->ship_seller = $request->input('ship_seller');
                $Order->total_shipment = $request->input('total_shipment');
                $Order->seller_total = $request->input('seller_total');
                $Order->buyer_total = $request->input('buyer_total');
                $Order->ship_method = $request->input('ship_method');
                $Order->tracking = $request->input('tracking');
                $Order->last_shipment_status = $request->input('last_shipment_status');
                $Order->shipment_status_time = $request->input('shipment_status_time');
                $Order->save();             
                return $this->successResponse($Order);
            }            
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }        
    }
}
