<?php
namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Page;
use Validator;
use Exception;
use Illuminate\Validation\ValidationException;  
  
class PageController extends Controller{    
    
    /*
     * Validate the inputs  * 
     */
    protected function orderValidateRules($request){
        $rules = [
            'title' => 'required|string',
            'slug' => 'required|unique:pages',            
            'content' => 'required|alpha_num',
            'isactive' => 'required|string',
            'showat' => 'required|string',
            'is_homepage' => 'required|string',
        ];         
        $validator = Validator::make($request, $rules, $this->errorMessage());                                   
        if($validator->fails()) {                  
            $messages = $validator->errors();            
            return $this->errorResponse($validator->errors());                
        }
        return true;   
    }
    /*
     * @param $request - Requests the inputs
     * Fetch all the records
     */
    public function index(Request $request){       
        
        try{          
            $r = $this->inputFilters($request->all());   
            $Page  = Page::where($r['filter'])->orderBy($r['sort'], $r['sortby'])->paginate($r['perpage']);                
            return $this->successResponse($Page);
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }       
    }

    /*
     * @param $id
     * Fetch the records by $id
     */
    public function getId($id){       
        //User::where('name', 'like', 'b%')->get();
        try{        
            if ($Page  = Page::find($id)) return $this->successResponse($Page);
            return $this->notFoundResponse();
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }
    }

    /*
     * @param $slug
     * Fetch the records by $slug
     */
    public function getSlug($slug){               
        try{           
            if ($Page  = Page::where('slug', '=', $slug)->first()) return $this->successResponse($Page);	  
            return $this->notFoundResponse();
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }
    }
  
    /*
     * @param $request
     * push the $request data in the table
     */
    public function create(Request $request){
        try{
            $request->request->add(['slug' => str_slug($request->input('title'), "-")  ]);
            if($this->orderValidateRules($request->all()) === true){                
                $Page = Page::create($request->all());  
                return $this->createdResponse($Page);
            }
        } catch(\Exception $exception){
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        } 
    }
  
    /*
     * @param $id
     * delete the record by $id
     */
    public function delete($id){      
        if (!$Page  = Page::find($id)) 
            return $this->notFoundResponse();		 
        try{               
            $Page->delete(); 
            return $this->deleteResponse();
        } catch(\Exception $exception){
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }         
    }
  
    /*
     * @param $id
     * @param $request
     * update $request by $id
     */
    public function update(Request $request,$id){        
        if (!$Page  = Page::find($id)) {
			return $this->notFoundResponse();
		}
        try{
            $request->request->add(['slug' => str_slug($request->input('title'), "-")  ]);
            if($this->orderValidateRules($request->all()) === true){                 
                $Page->title = $request->input('title');
                $Page->slug = $request->input('slug');
                $Page->content = $request->input('content');
                $Page->isactive = $request->input('isactive');
                $Page->showat = $request->input('showat');
                $Page->showin_homepage = $request->input('showin_homepage');
                $Page->save();  
                return $this->successResponse($Page);
            }
        } catch(\Exception $exception) {
            return $this->errorResponse(['exception' => $exception->getMessage()]);
        }        
    }
}
