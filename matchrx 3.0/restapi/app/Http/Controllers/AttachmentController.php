<?php

namespace App\Http\Controllers;
use App\Page;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use Exception;
use Illuminate\Validation\ValidationException;
  
  
class AttachmentController extends Controller{       

    /*
     * @param $request
     * push the $request data in the table
     */
    public function create(Request $request){
        try{             
            if ($request->hasFile('file') && $request->file('file')->isValid()) {
                $file = $request->file('file') ;
                $file_name = round(microtime(true) * 1000).time().'.'.$file->getClientOriginalExtension();               
                //$file->move(base_path('public/temp'),$file_data['name']);
                //$file->move($destinationPath, $picName);
                //$file_data = $this->getImageName($file);                
                //$request->request->add(['extension' => $file_data['extension']]);
                //$request->request->add(['name' => $file_data['name']]);
                //$file->move(,$file_data['name']);
                //pr($request->all()); 
            }
            
            exit;             
            
        } catch(\Exception $exception) {           
			return $this->errorResponse(['exception' => $exception->getMessage()]);
        }       
    }

    protected function getImageName($file){
        $data = [];        
        //$data['originalName'] =  $file->getClientOriginalName();
        $data['extension'] =  $file->getClientOriginalExtension();
        $data['size'] =  $file->getClientSize();
        $data['name'] =  round(microtime(true) * 1000).time().'.'.$file->getClientOriginalExtension(); 
               
        return $data;        
    }    
}
