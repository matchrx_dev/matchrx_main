<?php

namespace App\Http\Middleware;

use Closure;
use Log;
class CustomMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {				
		
		// 1. Change empty strings "" to null.
		$input = $request->all();
        if ($input) {
            array_walk_recursive($input, function (&$item, $key) {
                // RULES 1 FOR STRING AND PASSWORD
                /* if (is_string($item) && !str_contains($key, 'password')) {
                    $item = trim($item);
                } */
                // RULES 2 FOR NULL VALUE
                $item = ($item == "") ? null : $item;
			});
			// 2. Trim all the input values.
			$request->merge(array_map('trim', $input));            
        }
		return $next($request);
	}
}
