<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
  
class Page extends Model
{
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'title', 
        'slug', 
        'content',
        'isactive',
        'showat',
        'showin_homepage'        
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];     
}
