<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
  
class Order extends Model
{
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
		'order_currency',
        'order_status', 
        'return_label', 
        'exchange_label',
        'seller_reconfirm',
        'buyer_cancelled',
        'order_date',
		'buyer_name',
        'buyer_state', 
        'seller_name', 
        'seller_state',
        'seller_ar',
        'product_cost',
        'fee_seller',
		'ship_buyer',
        'ship_seller', 
        'total_shipment', 
        'buyer_total',
        'seller_total',
        'ship_method',
        'tracking',
		'last_shipment_status',
        'shipment_status_time',                
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'created_at',
        'updated_at'
    ];     
}
