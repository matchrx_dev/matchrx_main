<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

/*
 * Generate token 
 */
$router->group(['prefix' => 'api/v1'], function($router)
{
    $router->get('oauth/token', function () {
        return str_random(60);
    });

    $router->post('user/login','UserController@login');
});
 
$router->group(['prefix' => 'api/v1'], function($router)
{
    /*    
    * [ Attachment ] - GET, POST, PUT, DELETE
    */
    $router->post('attachments','AttachmentController@create');
    /*    
    * [ Pages ] - GET, POST, PUT, DELETE
    */
    $router->get('pages','PageController@index');
    
    $router->get('pages/{id:\d+}','PageController@getId');

    //$app->get('pages/{slug}','PageController@getSlug');
        
    $router->post('pages','PageController@create');
        
    $router->put('pages/{id}','PageController@update');
        
    $router->delete('pages/{id}','PageController@delete');
    
    /*    
    * [ Orders ] - GET, POST, PUT, DELETE
    */
    $router->get('orders','OrderController@index');   
    
    $router->get('orders/{id}','OrderController@getId');    
        
    $router->post('orders','OrderController@create');
        
    $router->put('orders/{id}','OrderController@update');
        
    $router->delete('orders/{id}','OrderController@delete');
     
});
 
