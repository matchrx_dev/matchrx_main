import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import 'rxjs/add/operator/switchMap';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { HttpModule } from '@angular/http';
/* [ App Component ] */
import { AppComponent } from './app.component';
/* [ Service ] */
import { AppService } from './app.service';
/* [ Custom Pipes ] */
import { JsonPipe, CapitalizePipe} from './app.pipe';
/* [ Custom Routing ] */
import { AppRoutingModule, routingComponents } from './app.routing';
@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    JsonPipe,
    CapitalizePipe,    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
