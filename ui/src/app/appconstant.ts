export const CONST = Object.freeze({
  ROUTES :{
    SIGNUP : 'regiser',
    SIGNIN : 'login',       
  },
  BASE_API_URL: 'http://example.com/',
  MYSITE :{
    NAME : 'vignesh',
    TEST :{
      APP : '123'
    }
  }
});
