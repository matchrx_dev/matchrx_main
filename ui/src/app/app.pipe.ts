import { Pipe, PipeTransform } from '@angular/core';
/*
** Stringify the Json Data
*/
@Pipe({ name: 'json' })
export class JsonPipe implements PipeTransform {
  transform(value: any): any {
    return JSON.stringify(value);
  }
}
/*
** capitalize every first letter of the word
*/
@Pipe({ name: 'capitalize' })
export class CapitalizePipe implements PipeTransform {
  transform(value: string, args: string[]): any {
    if (!value) return value;
    return value.replace(/\w\S*/g, function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }
}
