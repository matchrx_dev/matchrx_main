import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';

// Observable class extensions
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/switchMap';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AppService {

  _headers: Headers;  

  _headersAccept: string = 'application/json';

  _options: RequestOptions;

  _serverErrorMessage: string = 'Server error';

  constructor(private http: Http) {    
    this._headers = new Headers();        
    this._headers.append('content-type', 'application/x-www-form-urlencoded;application/json;charset=utf-8');
    this._options = new RequestOptions({ headers: this._headers });   
  }

  private extractData(res: Response) {    
    return res.json() || {};
  }

  private handleError(error: Response | any) {    
    const errMsg = (error.message) ? error.message : error.status ? 
      `${error.status} - ${error.statusText}` : this._serverErrorMessage;
    return Observable.throw(errMsg);
  }

  private handleErrorPromise(error: Response | any) {
    console.error(error.message || error);
    return Promise.reject(error.message || error);
  }

  /*
   * Method - DELETE
   * URL - Pass the Url
   * Key - Database Field Name
   * Val - Database Field Value 
   */
  deleteWithQueryTerm(url: string, key: string, val: string): Observable<any> {
    return this.http
      .delete(url + "/?" + key + "=" + val, this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /*  
   * Method - DELETE 
   * param  - Passing Input Values in the {}  
   */
  deleteService(url: string, param: any): Observable<any> {
    const params: URLSearchParams = new URLSearchParams();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {        
        params.set(key, param[key]);
      }
    }
    this._options = new RequestOptions({ headers: this._headers, search: params });
    return this.http
      .delete(url, this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /*  
   * Method - DELETE 
   * param  - Passing Input Values in the {}  
   */
  delete(url: string): Observable<any> {   
    return this.http
      .delete(url, this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }


  /*  
   * Method - GET 
   */
  getAll(url) {    
    return this.http
      .get(url, this._options)
      .map(this.extractData)
      .catch(this.handleError);    
  }

  /*  
   * Method - GET
   * With Promise 
   */
  getAllWithPromise(url) {    
    return this.http
      .get(url, this._options)
      .toPromise()
      .then(this.extractData)
      .catch(this.handleErrorPromise);    
  }

  /*  
   * Method - POST 
   * param  - Passing Input Values in the {}  
   */
  create(url: string, param: any): Observable<any> {     
    return this.http
      .post(url, JSON.stringify(param), this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /*  
   * Method - PUT
   * param  - Passing Input Values in the {}  
   */
  update(url: string, param: any): Observable<any>{        
    return this.http
      .put(url, JSON.stringify(param), this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /*  
   * Method - PATCH
   * param  - Passing Input Values in the {}  
   */
  patch(url: string, param: any): Observable<any> {
    
    return this.http
      .patch(url, JSON.stringify(param), this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /*
   * Method - GET
   * URL - Pass the Url
   * Key - Database Field Name
   * Val - Database Field Value 
   */
  getDataWithQueryTerm(url: string, key: string, val: string): Observable<any> {
    return this.http
      .get(url + "/?" + key + "=" + val, this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }
  /*
   * Method - GET
   * URL - Pass the Url
   * param - passing field and value inside the {}   
   */
  getDataWithFixedQueryString(url: string, param: any): Observable<any> {
    this._options = new RequestOptions({ headers: this._headers, search: 'query=' + param });
    return this.http
      .get(url, this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }

  /*
   * Method - GET
   * URL - Pass the Url
   * param - passing field and value inside the {}
   */
  getWithComplexObjectAsQueryString(url: string, param: any): Observable<any> {
    const params: URLSearchParams = new URLSearchParams();
    for (const key in param) {
      if (param.hasOwnProperty(key)) {        
        params.set(key, param[key]);
      }
    }
    this._options = new RequestOptions({ headers: this._headers, search: params });
    return this.http
      .get(url, this._options)
      .map(this.extractData)
      .catch(this.handleError);
  }
}