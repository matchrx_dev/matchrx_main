/* [ Core Library ] */
import { NgModule, Component, OnInit } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap } from '@angular/router';
/* [ Get Constant ] */
import { CONST } from './appconstant';
/* [ Custom Component ] */
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { ListComponent } from './components/list/list.component';
import { CreateComponent } from './components/create/create.component';
import { UsersComponent } from './components/users/users.component';

/* [ Routing ] */
const appRoutes: Routes = [
  { path: '', redirectTo: 'users', pathMatch: 'full',},
  { 
    path: 'users', component: UsersComponent, 
    children:[
      { path: '', pathMatch: 'full', redirectTo: 'list' },
      { path : 'list', component: ListComponent },
      { path : 'create', component: CreateComponent },
      { path : 'update/:id', component: CreateComponent },
    ]
  },
  { path: CONST.ROUTES.SIGNUP, component: SignupComponent },
  { path: CONST.ROUTES.SIGNIN, component: SigninComponent },        
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: false, useHash: false } /* [ For debugging ourpose only ] */
    )
  ],
  exports: [RouterModule]
})

export class AppRoutingModule { }

export const routingComponents = [
  HomeComponent,
  HeaderComponent,
  FooterComponent,  
  SigninComponent,
  SignupComponent,
  ListComponent,
  CreateComponent, 
  UsersComponent
];