import { Component } from '@angular/core';
import { AppComponent } from '../../app.component';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HeaderComponent } from '../header/header.component';
import { FooterComponent } from '../footer/footer.component';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css'],
  providers : [HeaderComponent,FooterComponent]
})
export class SigninComponent extends AppComponent {  

}
