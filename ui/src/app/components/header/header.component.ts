import { Component } from '@angular/core';
import { AppComponent } from '../../app.component';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent extends AppComponent {
  pageTitle = 'Angular Js';

  constructor(
      activatedRoute: ActivatedRoute,
      router: Router,
      appService: AppService
  ) {
      super(
          activatedRoute,
          router,
          appService
      );     
  }
}
