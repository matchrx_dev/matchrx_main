import { Component, Injectable } from '@angular/core';
import { UsersComponent } from '../users/users.component';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap, Params } from '@angular/router';
import { AppService } from '../../app.service';
import { Http, Response, Headers, RequestOptions, URLSearchParams } from '@angular/http';


@Component({
    selector: 'app-create',
    templateUrl: './create.component.html',
    styleUrls: ['./create.component.css']
})
export class CreateComponent extends UsersComponent {

    paramId: number = null;

    frmUser: object = {};

    showErrorMsg:any = null;

    constructor(
        activatedRoute: ActivatedRoute,
        router: Router,
        appService: AppService,
        private http: Http
    ) {
        super(
            activatedRoute,
            router,
            appService
        );
        activatedRoute.paramMap.subscribe((params: Params) =>
        { this.paramId = params.get("id"); }
        );
        this.getUsersById();
    }

    getUsersById() {
        if (this.paramId !== null) {
            this.appService
                .getAllWithPromise(this.gUsers(this.paramId))
                .then((res) => {
                    if (res.code === 200 && res.data !== '') {
                        this.frmUser = res.data;
                    }
                }, err => {

                });
        }
    }

    submitUser(value: any, valid: boolean) {
        /* [ Create New User ] */
        if (valid === true && this.paramId === null) {            
            this.appService.create(this.gUsers('register'), value)
                .subscribe(result => {
                    if(result.code === 201 && result.data !== ''){
                        this.router.navigateByUrl('/users/list');
                    } else if(result.code === 422 && result.data !== ''){
                        this.showErrorMsg = this.dispErrorMsg(result);
                    }                    
                }, error => {

                });            
        /* [ Update the existing User ] */
        } else {           
            this.appService.update(this.gUsers() + '/'+ this.paramId, value)
                .subscribe(result => {
                    //console.log('res',result);
                    if(result.code === 200 && result.data !== ''){
                        this.router.navigateByUrl('/users/list');
                    } else if(result.code === 422 && result.data !== '') {
                        this.showErrorMsg = this.dispErrorMsg(result.data);                        
                    } 
                }, error => {

                });
                
        }
    }    
}
