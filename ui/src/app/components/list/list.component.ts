import { Component } from '@angular/core';
import { UsersComponent } from '../users/users.component';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../../app.service';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent extends UsersComponent {

    users: any = [];

    constructor(
        activatedRoute: ActivatedRoute,
        router: Router,
        appService: AppService
    ) {
        super(
            activatedRoute,
            router,
            appService
        );
        this.getUsers();
    }
    
    /* [ Get the Users ] */
    getUsers() {
        this.appService
            .getAllWithPromise(this.gUsers())
            .then((res) => {
                if (res.code === 200 && res.data.data.length > 0) {
                    this.users = res.data.data;
                }
            }, err => {

            });
    }
    /* [ Delete the User ] */
    onDelete(event: any, id: number) {
        if (id && confirm("Are you confirm to delete?") == true) {
            this.appService
                .delete(this.gUsers() + '/'+ id)
                .subscribe(result => {
                    if(result.code === 200 && result.data !== ''){
                        window.location.reload();
                    }                    
                }, error => {

                });   
        }        
    }
}
