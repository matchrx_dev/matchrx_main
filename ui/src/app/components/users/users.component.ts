import { Component } from '@angular/core';
import { AppComponent } from '../../app.component';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../../app.service';

@Component({
    selector: 'app-users',    
    template: '<router-outlet></router-outlet>',
    styleUrls: ['./users.component.css']
})
export class UsersComponent extends AppComponent {

    endpoint: string = 'users';

    constructor(
        activatedRoute: ActivatedRoute,
        router: Router,
        appService: AppService
    ) {
        super(
            activatedRoute,
            router,
            appService
        );
    }

    gUsers(q:any = null){
        let url = this.apiUrl + this.endpoint;
        if(q) return url + '/'+q;
        return url;
    }    
}
