import { Component } from '@angular/core';
import { AppComponent } from '../../app.component';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap } from '@angular/router';
import { AppService } from '../../app.service';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.css']
})
export class UpdateComponent extends AppComponent { 

  constructor(
      activatedRoute: ActivatedRoute,
      router: Router,
      appService: AppService
  ) {
      super(
          activatedRoute,
          router,
          appService
      );     
  }
}
