import { Component, Input, EventEmitter, Output, OnInit, Injectable } from '@angular/core';
import { RouterModule, Routes, Router, ActivatedRoute, ParamMap, Params } from '@angular/router';
import { HttpModule } from '@angular/http';
import { AppService } from './app.service';
import { CONST } from './appconstant';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent {

  router: any;

  appService: any;

  activeRoutePath: string;

  routing: object = {};

  apiUrl: string = 'http://localhost/matchrx/restapi/api/v1/';

  constructor(
    activatedRoute: ActivatedRoute,
    router: Router,
    appService: AppService
  ) {
    this.activeRoutePath = activatedRoute.snapshot.url.join('');
    this.router = router;
    this.appService = appService;    
  }

  /* [ Get Constant Value ] */
  getConst(c = null) {
    if (c != null) return this.fetchFromObject(CONST, c);
    return CONST;
  }

  /* [ Fetch from Object ] */
  fetchFromObject(obj, prop) {
    if (typeof obj === 'undefined') return false;
    var _index = prop.indexOf('.')
    if (_index > -1) {
      return this.fetchFromObject(obj[prop.substring(0, _index)], prop.substr(_index + 1));
    }
    return obj[prop];
  }

  /* [ Set Route Path ] */
  getRoute(r){
    return '/'+ this.getConst(r);
  }

  /* [ Get image ] */
  getAssets(u){
    return 'assets/img/'+ u;
  }  

  /*
   * Return 'active' 
   * when current rout and requested route are same
   */
  gActiveClass(p){
    return (this.activeRoutePath === this.getConst(p)) ? 'active' : '';
  }

  /* 
   * Display Error Message
   */
  dispErrorMsg(result){
    let response= "<ul>";
    for (let i in result) {
        response+= "<li class='text-danger'>"+result[i].pop()+"</li>"; 
    }
    response+= "<ul>";
    return response;       
  }
}
